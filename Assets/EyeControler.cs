﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeControler : MonoBehaviour
{
    public Transform Target;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Target == null)
        {
            Target = GameObject.FindGameObjectWithTag("item").transform;
        }
        else
        {
            gameObject.transform.up = Target.position-gameObject.transform.position;
        }
    }
}
